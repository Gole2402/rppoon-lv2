using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv2
{
    class FileLogger : ILogger
    {
        private string filePath;

        public FileLogger(string pathFile)
        {
            this.filePath = pathFile;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(message);
            }
        }
    }
}
