using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv2
{
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}

        public Die (int numberofSides)
        {
            this.numberOfSides = numberofSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        } 



     



        //public Die(int numberOfSides, Random generator)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}
        //public int Roll()
        //{
        //    int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //}

        public int Roll()
        {
            return this.randomGenerator.NextInt(1, numberOfSides + 1);
        }
    }
}
