using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //DiceRoller diceRoller = new DiceRoller();
            //const int NumberOfDices = 20;
            //for (int i = 0; i < NumberOfDices; i++)
            //{
            //    diceRoller.InsertDie(new Die(6));
            //}
            //diceRoller.RollAllDice();
            //IList<int> results = diceRoller.GetRollingResults();
            //foreach (int result in results)
            //{
            //    Console.WriteLine(result);
            //}

            DiceRoller diceRoller = new DiceRoller();
            Random randomGenerator = new Random();

            const int NumberofDices = 20;
            for (int i = 0;i<NumberofDices;i++)
            {
                diceRoller.InsertDie(new Die(6,randomGenerator));
            }
            diceRoller.RollAllDice();
            IList<int> results = diceRoller.GetRollingResults();
            foreach (int result in results)
            {
                Console.WriteLine(result);
            }





        }
    }
}
